var canvasWorker.rasterAssembly = {

    draw : true ,

    layers : [ ] ,

    width : function ( ) {
        return canvasWorker.plotter.boundingBox.width == undefined ?
                               window.innerWidth : canvasWorker.plotter.boundingBox.width ;
    } ,

    height : function ( ) {
        return canvasWorker.plotter.boundingBox.height == undefined ?
                               window.innerHeight : canvasWorker.plotter.boundingBox.height ;
    } ,

    imageData : function ( millis ) {
        var canvas_ = new OffscreenCanvas( canvasWorker.rasterAssembly.width, canvasWorker.rasterAssembly.height ) ;
        if ( canvasWorker.rasterAssembly.draw ) {
            canvasWorker.rasterAssembly.raster ( ) ;
        }
        canvasWorker.rasterAssembly.assembly ( canvas_ ) ;
        return canvas_.getContext ( '2d' ).getImageData ( 0 , 0 , canvas_.width , canvas_.height ) ;
    } ,

    raster : function ( ) {
        canvasWorker.rasterAssembly.layers = [ ] ;
        for ( var i = 0 ; i < canvasWorker.plotter.renders.length ; i ++ ) {
            var render_ = canvasWorker.plotter.renders[ i ] ;
            if ( render_.selected ) {
                var drawings_ = canvasWorker.frameRequest.renderDrawings ( render_.name ) ;
                for ( var j = 0 ; j < drawings_.length ; j ++ ) {
                    canvasWorker.rasterAssembly.layer ( drawings_ [ j ] ) ;
                }
            }
        }
    } ,

    assembly : function ( canvas ) {
        for ( var i = 0 ; i < canvasWorker.rasterAssembly.layers.length ; i ++ ) {
            var layer_ = canvasWorker.rasterAssembly.layers[ i ]
            if ( canvasWorker.rasterAssembly.drawLayer ( layer_ , millis ) ) {
                canvas.drawImage ( layer_.canvas ) ;
            }
        }
    } ,

    layer : function ( drawing ) {
        var layer_ = canvasWorker.rasterAssembly.layerConfiguration ( drawing ) ;
        layer_.canvas.getContext ( '2d' ).putImageData ( drawing.imageData , 0 , 0 ) ;
        return layer_ ;
    } ,

    layerConfiguration : function ( drawing ) {
        for ( var i = 0 ; i < canvasWorker.rasterAssembly.layers.length ; i ++ ) {
            var layer_ = canvasWorker.rasterAssembly.layers [ i ] ;
            if ( drawing.layer == layer_.id && drawing.frame == layer_.frame  ) {
                return layer_ ;
            }
        }
        var layer_ = canvasWorker.rasterAssembly.initLayer ( drawing ) ;
        canvasWorker.rasterAssembly.layers.push ( layer_ ) ;
        return layer_ ;
    } ,

    initLayer : function ( drawing ) {
        return {
                     id : drawing.layer ,
                     frame : drawing.frame ,
                     delta : 0 ,
                     totalFrames : drawing.totalFrames ,
                     velocity : drawing.velocity ,
                     canvas : new OffscreenCanvas( canvasWorker.rasterAssembly.width , canvasWorker.rasterAssembly.height )
                 } ;
    } ,

    drawLayer : function ( layer , millis ) {
        var frame_ = canvasWorker.rasterAssembly.frame ( layer , millis ) ;
        return frame_ == layer.frame ;
    } ,

    frame : function ( layer , millis ) {
        layer.delta += new Date ( ).now ( ) - millis ;
        if ( layer.velocity < layer.delta * layer.totalFrames ) {
            layer.delta = 0 ;
        }
        return layer.totalFrames == 0 ? 0 : layer.delta % layer.totalFrames ;
    } ,

} ,