var canvasWorker.offscreenDrawing = {

    name : null ,

    layer : null ,

    bound : null ,

    velocity : 0 ,

    frame : null ,

    totalFrames : null ,

    transform : {
        scale : 1 ,
        x : 0 ,
        y : 0
    } ,

    boundingBox : {
        width: width ,
        height: height
    } ,

    style : {
        pen : 1 ,
        color : '#ffffff' ,
        alpha : 1
    } ,

    imageData : null ,

} ,