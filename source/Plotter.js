var canvasWorker.plotter = {

    url : null ,

    animationFrame : null ,

    renders : [ ] ,

    canvas : function ( ) {
        if ( canvasWorker.plotter.canvas_ == undefined ) {
            canvasWorker.plotter.canvas_ = document.createElement('canvas') ;
        }
        if ( canvasWorker.plotter.boundingBox.width == undefined ) {
            canvasWorker.plotter.canvas_.width = window.innerWidth;
            canvasWorker.plotter.canvas_.height = window.innerHeight;
        } else if ( canvasWorker.plotter.boundingBox.height == undefined ) {
            canvasWorker.plotter.canvas_.width = canvasWorker.plotter.boundingBox.width;
            canvasWorker.plotter.canvas_.height = canvasWorker.plotter.boundingBox.width;
        } else {
            canvasWorker.plotter.canvas_.width = canvasWorker.plotter.boundingBox.width;
            canvasWorker.plotter.canvas_.height = canvasWorker.plotter.boundingBox.height;
        }
        return canvasWorker.plotter.canvas_ ;
    } ,

    init : function ( element , url , width , height ) {
        canvasWorker.plotter.url = url ;
        canvasWorker.plotter.boundingBox = { width : width , height : height } ;
        element.appendChild ( canvasWorker.plotter.canvas ( ) ;
        canvasWorker.frameRequest.postMessage ( ) ;
    } ,

    draw : function ( ) {
        var context_ = canvasWorker.plotter.canvas ( ).getContext("2d") ;
        canvasWorker.plotter.clean ( ) ;
        context_.putImageData ( canvasWorker.animationFrame.frame ( ) , 0 , 0 ) ;
        canvasWorker.plotter.animationFrame = window.requestAnimationFrame ( canvasWorker.plotter.draw ) ;
    } ,

    pause : function ( ) {
        window.cancelAnimationFrame( canvasWorker.plotter.animationFrame ) ;
        canvasWorker.plotter.animationFrame = null ;
    } ,

    clean : function ( ) {
        var canvas_ = canvasWorker.plotter.canvas ( );
        var context_ = canvas_.getContext('2d') ;
        context_.clearRect ( 0 , 0 , canvas_.width , canvas_.height ) ;
    } ,

    render : function ( name ) {
        for ( var i = 0 ; i < canvasWorker.plotter.renders.length ; i ++ ) {
            if ( render[ i ].name == name )
                return render[ i ] ;
        }
    } ,

} ,