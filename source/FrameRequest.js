var canvasWorker.frameRequest = {

    drawings : [ ] ,

    workers : { } ,

    onmessage : function ( event ) {
        var drawing_ = canvasWorker.frameRequest
                            .eventDrawing ( event.data.name , event.data.layer , event.data.bound , event.data.frame ) ;
        drawing_.imageData = event.data.imageData ;
    } ,

    postMessage : function ( ) {
      for ( var i = 0 ; i < canvasWorker.plotter.renders.length ; i ++ ) {
        var render_ = canvasWorker.plotter.renders[ i ] ;
        if ( render_.selected ) {
            var drawings_ = canvasWorker.frameRequest.renderDrawings ( render_.name );
            for ( var j = 0 ; j < drawings_.length ; j ++ ) {
                var drawing_ = drawings_[ j ];
                if ( canvasWorker.frameRequest.transform ( drawing_ ) ) {
                    drawing_.transform = canvasWorker.frameRequest.drawingTransform ( ) ;
                    drawing_.boundingBox = canvasWorker.frameRequest.drawingBoundingBox ( ) ;
                    drawing_.style = render_.style;
                    drawing_.interaction = canvasWorker.plotter.interaction ;
                    canvasWorker.frameRequest.workers[ render_.name ].postMessage ( drawing_ ) ;
                }
            }
        }
      }
      window.requestAnimationFrame( canvasWorker.frameRequest.postMessage );
    } ,

    requestWorker : function ( name ) {
        if ( canvasWorker.frameRequest.workers [ name ] == undefined ) {
            canvasWorker.frameRequest.workers [ name ] = new Worker ( canvasWorker.plotter.url) ;
            canvasWorker.frameRequest.workers [ name ].onmessage = canvasWorker.frameRequest.onmessage ;
            canvasWorker.frameRequest.workers [ name ].postMessage ( { 'config'} ) ;
        }
        return canvasWorker.frameRequest.workers [ name ] ;
    } ,

    renderDrawings : function ( name ) {
        var drawings_ = [ ] ;
        for ( var i = 0 ; i < canvasWorker.frameRequest.drawings.length ; i ++ ) {
            if ( canvasWorker.frameRequest.drawings[ i ].name == name ) {
                drawings_.push ( canvasWorker.frameRequest.drawings[ i ] ) ;
            }
        }
        return drawings_ ;
    } ,

    eventDrawing : function ( name , layer , bound , frame ) {
        for ( var i = 0 ; i < canvasWorker.frameRequest.drawings.length ; i ++ ) {
            var drawing_ = canvasWorker.frameRequest.drawings[ i ] ;
            if ( drawing_.name == name && drawing_.layer == layer
                    && drawing_.bound == bound && drawing_.frame == frame ) {
                return drawing_ ;
            }
        }
    } ,

    transform : function ( drawing ) {
        var render_ = canvasWorker.plotter.render ( drawing.name ) ;
        return drawing.transform.scale != canvasWorker.canvasTransform.scale
                || drawing.transform.x != canvasWorker.canvasTransform.x
                || drawing.transform.y != canvasWorker.canvasTransform.y
                || drawing.style.pen != render.style.pen
                || drawing.style.color != render.style.color
                || drawing.style.alpha != render.style.alpha
                || drawing.boundingBox.width != ( canvasWorker.plotter.boundingBox.width == undefined ?
                                                        window.innerWidth : canvasWorker.plotter.boundingBox.width )
                || drawing.boundingBox.height != ( canvasWorker.plotter.boundingBox.height == undefined ?
                                                        window.innerHeight : canvasWorker.plotter.boundingBox.height )
                || render_.transform ;

    } ,

    drawingTransform : function ( ) {
        return {
                   scale : canvasWorker.canvasTransform.scale ,
                   x : canvasWorker.canvasTransform.x ,
                   y : canvasWorker.canvasTransform.y
               } ;
    } ,

    drawingBoundingBox : function ( ) {
        return {
                    width : canvasWorker.plotter.boundingBox.width == undefined ?
                                            window.innerWidth : canvasWorker.plotter.boundingBox.width ,
                    height : canvasWorker.plotter.boundingBox.height == undefined ?
                                             window.innerHeight : canvasWorker.plotter.boundingBox.height
               } ;
    } ,

} ,